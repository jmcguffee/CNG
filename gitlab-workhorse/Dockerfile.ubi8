ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_VERSION=v13.8.0
ARG GITLAB_USER=git
ARG UID=1000
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-workhorse" \
      name="GitLab Workhorse" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Gitlab Workhorse is a smart reverse proxy for GitLab." \
      description="Gitlab Workhorse is a smart reverse proxy for GitLab. It handles large HTTP requests."

ENV LANG=C.UTF-8

ADD gitlab-workhorse-ee.tar.gz /
ADD gitlab-gomplate.tar.gz /

COPY scripts/ /scripts/

RUN dnf ${DNF_OPTS} install -by --nodocs perl \
    && dnf clean all \
    && rm -r /var/cache/dnf \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    && mkdir -p /var/log/gitlab /srv/gitlab/config ${GITLAB_DATA} \
    && chown -R ${UID}:0 \
        /srv/gitlab \
        /var/log/gitlab \
        ${GITLAB_DATA} \
        /home/${GITLAB_USER} \
    && chmod -R g=u \
        /srv/gitlab \
        /var/log/gitlab \
        ${GITLAB_DATA} \
        /home/${GITLAB_USER} \
        /scripts

USER ${UID}

CMD /scripts/start-workhorse

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
